import * as AppDrift from "@appdrift/core";
import { TSFScrolling } from "./TSFScrolling";

export class SpaceForscrolling extends AppDrift.Container {
    public scrollEnabled: boolean = false;
    public visible: boolean = true;
    public style: any = {
        height: 350
    };
    public containerStyle: any = {};

    public render() {
        return (
            <AppDrift.Fragment>
                <TSFScrolling />
            </AppDrift.Fragment>
        );
    }
}
