import * as AppDrift from "@appdrift/core";
import { StatusBar } from "@appdrift/services";
import { Animation } from '@appdrift/services';


export class app extends AppDrift.App {
    public readonly initialScreen: string = "Screen1";
    public readonly scheme: Array<string> = [];

    public onDeepLink(path: string, params: { [param: string]: string }, uri: string) {
    }

    constructor() {
        super();

        StatusBar.setBarStyle('light-content');
        StatusBar.setBackgroundColor('black');
        Animation.enableAnimations();
    }
}
