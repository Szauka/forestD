import * as AppDrift from "@appdrift/core";

export class Datepicker extends AppDrift.DateTimePicker {
    public style = {
        backgroundColor: '#f05454',
        height: 150,
        width: '100%'
    }
    // public mode = 'date' as any;

    public get show() {
        return true;
    }

}
