import * as AppDrift from "@appdrift/core";

export class SendEmailText extends AppDrift.Text {
    public style: any = {
        color: 'white',
        textAlign: 'center'
    };
    public text: string = 'Send Email';
}
