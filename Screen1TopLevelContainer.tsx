import * as AppDrift from "@appdrift/core";
import { HeaderContainer } from "./HeaderContainer";
import { Body } from "./Body";

export class Screen1TopLevelContainer extends AppDrift.Container {
    public scrollEnabled: boolean = false;
    public visible: boolean = true;
    public style: any = {
        flex: 1,
        // backgroundColor: "blue"
    };
    public containerStyle: any = {};

    public render() {
        return (
            <AppDrift.Fragment>
                <HeaderContainer />
                <Body />
            </AppDrift.Fragment>
        );
    }
}
