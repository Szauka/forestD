import * as AppDrift from "@appdrift/core";
import { ListItem, IListItem } from "./ListItem";

export class ListComponents extends AppDrift.List {
    public items: Array<IListItem> = [
        { text: 'One', color: 'red' },
        { text: '2', color: 'yellow' },
        { text: '3', color: 'blue' },
        { text: '4', color: 'red' },
        { text: '5', color: 'yellow' },
        { text: '6', color: 'blue' },
        { text: '7', color: 'red' },
    ];

    public renderItem(item: IListItem) {
        return (
            <AppDrift.Fragment>
                <ListItem text={item.text} color={item.color} />
            </AppDrift.Fragment>
        );
    }
}

