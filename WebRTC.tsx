import * as AppDrift from "@appdrift/core";
import { VOIP } from "@appdrift/services";
import { Store } from "@appdrift/core";

export class WebRTC extends AppDrift.WebRtc {


    public _streamURL = Store({ url: undefined });
    
    public mirror = true;

    public get streamURL() {
        return this._streamURL.url;
    };
    public async mystream() {
        this._streamURL.url = await VOIP.getMyCameraStream();
    }
    constructor(props) {
        super(props);
        this.mystream();
    }

    public style = {
        position: 'absolute',
        top: 73,
        right: 10,
        width: 100,
        height: 160,
    }

    public objectFit = 'cover';

}
