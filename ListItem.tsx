import * as AppDrift from "@appdrift/core";
import { ListItemText } from "./ListItemText";

export interface IListItem {
    text: string,
    color: string,
}

export class ListItem extends AppDrift.Container<IListItem> {
    public scrollEnabled: boolean = false;
    public visible: boolean = true;
    public style: any = {
        height: 30,
        margin: 10,
        width: '80%',
        backgroundColor: this.props.color,
    };
    public containerStyle: any = {};

    public render() {
        return (
            <AppDrift.Fragment>
                <ListItemText textValue={this.props.text} />
            </AppDrift.Fragment>
        );
    }
}
