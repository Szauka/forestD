import * as AppDrift from "@appdrift/core";
import { BackIcon } from "./BackIcon";
import { HeaderTopText } from "./HeaderTopText";

export class HeaderTopContainer extends AppDrift.Container {
    public scrollEnabled: boolean = false;
    public visible: boolean = true;
    public style: any = {
        display: 'flex',
        flexDirection: 'row',
        flex: 1,
        backgroundColor: '#3797a4'
    };
    public containerStyle: any = {};

    public render() {
        return (
            <AppDrift.Fragment>
                <BackIcon />
                <HeaderTopText />
            </AppDrift.Fragment>
        );
    }
}
