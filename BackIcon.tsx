import * as AppDrift from "@appdrift/core";
import { SafeView } from "@appdrift/services";


export class BackIcon extends AppDrift.Icon {
    public iconSet: IconSetType = 'ionicons';
    public name: IconTypes.Ionicons = 'ios-add';
    public size: number = 30;
    public color: string = 'white';
    public style: any = {
        paddingTop: SafeView.insets.top,
        paddingLeft: 10
    };
}
