import * as AppDrift from "@appdrift/core";
import { Top } from "./Top";
import { Bottom } from "./Bottom";

export class Body extends AppDrift.Container {
    public scrollEnabled: boolean = false;
    public visible: boolean = true;
    public style: any = {
        flex: 1,
    };
    public containerStyle: any = {};

    public render() {
        return (
            <AppDrift.Fragment>
                <Top />
                <Bottom />
            </AppDrift.Fragment>
        );
    }
}
