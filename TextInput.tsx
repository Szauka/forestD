import * as AppDrift from "@appdrift/core";

export class TextInput extends AppDrift.TextInput {

    public placeholder: string = 'Enter text here';
    public placeholderTextColor: string = 'white';
    public multiline: boolean = false;
    public style: any = {
        borderColor: 'gold',
        borderWidth: 1,
        padding: 5
    };
    public value = '';

    constructor(props) {
        super(props);

        this.value = 'Testing';
    }

    public onChange = (text: string) => {

        console.log('TEXT:', text);

        this.value = text;
    }
}
