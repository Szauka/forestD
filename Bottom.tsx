import * as AppDrift from "@appdrift/core";
import { CenteredButton } from "./CenteredButton";
import { Datepicker } from "./Datepicker";
import { Image } from "./Image";
import { Map } from "./Map";
import { KeyboardAwareView } from "./KeyboardAwareView";
import { SpaceForscrolling } from "./SpaceForscrolling";
import { SendEmail } from "./SendEmail";
import { WebRTC } from "./WebRTC";
import { ListComponents } from "./ListComponents";

export class Bottom extends AppDrift.Container {
    public scrollEnabled: boolean = true;
    public visible: boolean = true;
    public style: any = {
        flex: 1
    };
    public containerStyle: any = {
        // flex: 1,
        backgroundColor: '#3797a4',
        justifyContent: 'center',
        alignItems: 'center',
    };

    _onClick = () => {
        alert(` 🙃 `);
    }

    public render() {
        return (
            <AppDrift.Fragment>
                <CenteredButton
                    onClick={this._onClick}
                    buttonText={'Press'}
                    bgColor='#59886b'
                />
                <Datepicker />
                <Image />
                <Map />
                <KeyboardAwareView />
                <SpaceForscrolling />
                <SendEmail />
                <WebRTC />
                <ListComponents />
            </AppDrift.Fragment>
        );
    }
}
