import * as AppDrift from "@appdrift/core";

export class Image extends AppDrift.Image {
    public image: string = 'https://www.tourismeloiret.com/en/sites/site.en/files/styles/medium_970x970/public/upload/decouvrir/les-12-incontournables/la-foret-d-orleans/diaporama/chenes-en-foret-d-orleans.jpg?itok=1qu0PZ9E';
    public style: any = { height: 300, width: 300 };
    public containerStyle: any = {};
}
