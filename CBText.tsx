import * as AppDrift from "@appdrift/core";

export class CBText extends AppDrift.Text {
    public style: any = {
        color: 'white'
    };
    public text: string = this.props.text;
}
