import * as AppDrift from "@appdrift/core";
import { Screen1TopLevelContainer } from "./Screen1TopLevelContainer";

export class Screen1 extends AppDrift.Screen {
    public render() {
        return (
            <AppDrift.Fragment>
                <Screen1TopLevelContainer />
            </AppDrift.Fragment>
        );
    }
}
