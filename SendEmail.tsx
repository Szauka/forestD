import * as AppDrift from "@appdrift/core";
import { SendEmailText } from "./SendEmailText";
import { MailComposer } from '@appdrift/services';

export class SendEmail extends AppDrift.Container {
    public scrollEnabled: boolean = false;
    public visible: boolean = true;
    public style: any = {
        height: 30,
        margin: 10,
        width: '80%',
        backgroundColor: '#52057b',
        marginBottom: 50 
    };
    public containerStyle: any = {};

    async onPress() {

        console.log('Test');

        const available = await MailComposer.isAvailable();

        console.log(available);

        // if (available) {
            await MailComposer.compose({
                // attachments: [FileSystem.documentDirectory + fileName],
                subject: 'From forest D',
                body: `Please blah ... `,
            });
        // }
    }

    public render() {
        return (
            <AppDrift.Fragment>
                <SendEmailText />
            </AppDrift.Fragment>
        );
    }
}
