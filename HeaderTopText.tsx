import * as AppDrift from "@appdrift/core";

export class HeaderTopText extends AppDrift.Text {
    public style: any = {
        color: 'white'
    };
    public text: string = 'Go Back';
}
