import * as AppDrift from "@appdrift/core";
import { CBText } from "./CBText";
import { Alert, } from "@appdrift/services";

export interface ICenteredButton {
    onClick: () => void;
    bgColor: string;
    buttonText: string;
}

export class CenteredButton extends AppDrift.Container<ICenteredButton> {
    public scrollEnabled: boolean = false;
    public visible: boolean = true;
    public style: any = {
        backgroundColor: this.props.bgColor,
        padding: 10
    };
    public containerStyle: any = {};

    public static index = 0;

    onPress = (e) => {

        console.log('this.props.buttonTex', this.props.buttonText);
        this.props.onClick();

        // if (CenteredButton.index === 0) {
        //     alert(`You've clicked me 🙃 \n Try again!`);
        //     CenteredButton.index++;
        // } else if (CenteredButton.index === 1) {
        //     CenteredButton.index--;
        //     Alert.alert(
        //         `Senior Alert, have my own title!`,
        //         'Try clicking again, what could happen?',
        //         // [
        //         //     {
        //         //         text: "Cancel",
        //         //         onPress: () => console.log("Cancel Pressed"),
        //         //         style: "cancel"
        //         //     },
        //         //     { text: "OK", onPress: () => console.log("OK Pressed") }
        //         // ]
        //     )
        // }
    }

    public render() {
        return (
            <AppDrift.Fragment>
                <CBText text={this.props.buttonText} />
            </AppDrift.Fragment>
        );
    }
}
