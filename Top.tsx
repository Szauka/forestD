import * as AppDrift from "@appdrift/core";
import { CenteredButton } from "./CenteredButton";

export class Top extends AppDrift.Container {
    public scrollEnabled: boolean = false;
    public visible: boolean = true;
    public style: any = {
        backgroundColor: '#fcf876',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    };
    public containerStyle: any = {};

    _onClick = () => {
        alert(`You've clicked me 🙃 \n Try again!, from top`);
    }

    public render() {
        return (
            <AppDrift.Fragment>
                <CenteredButton
                    onClick={this._onClick}
                    buttonText={'Press Me'}
                    bgColor='#3797a4'
                />
            </AppDrift.Fragment>
        );
    }
}
