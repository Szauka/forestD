import * as AppDrift from "@appdrift/core";

export interface IListItemText {
    textValue: string
}

export class ListItemText extends AppDrift.Text<IListItemText> {
    public style: any = {
        color: 'navy'
    };

    public text: string = this.props.textValue;
}
