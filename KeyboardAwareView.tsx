import * as AppDrift from "@appdrift/core";
import { TextInput } from "./TextInput";

export class KeyboardAwareView extends AppDrift.KeyboardAvoidingContainer {
    public visible: boolean = true;
    // public style: any = { flex: 1 };

    public render() {
        return (
            <AppDrift.Fragment>
                <TextInput />
            </AppDrift.Fragment>
        );
    }
}
