import * as AppDrift from "@appdrift/core";
import { HeaderTopContainer } from "./HeaderTopContainer";

export class HeaderContainer extends AppDrift.Container {
    public scrollEnabled: boolean = false;
    public visible: boolean = true;
    public style: any = {
        backgroundColor: '#cee397',
        height: 120, 
        display: 'flex'
    };
    public containerStyle: any = {};

    public render() {
        return (
            <AppDrift.Fragment>
                <HeaderTopContainer />
            </AppDrift.Fragment>
        );
    }
}
