import * as AppDrift from "@appdrift/core";

export class Map extends AppDrift.Map {
    public initialRegion: any = {
        latitude: 46.0678639,
        latitudeDelta: 0.008,
        longitude: 20.9520523,
        longitudeDelta: 0.0421
    };

    public markers: Array<IMapMarker> = [];
    public style: any = {
        height: 300,
        width: 300
    };
    public containerStyle: any = [];
}
