import * as AppDrift from "@appdrift/core";

export class TSFScrolling extends AppDrift.Text {
    public style: any = {
        fontSize: 50,
        paddingTop: 30
    };
    public text: string = 'Just text to allow scrolling!';
}
